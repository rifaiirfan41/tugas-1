import java.util.Scanner;

public class TugasSatuIrfan {
    public static void main(String[] args) {
        System.out.println("Pilihan Fitur :\n1. Identitas \n2. Kalkulator \n3. Perbandingan \n4. Keluar");
        Scanner inputuser = new Scanner(System.in);
        System.out.print("\nPilih fitur : ");
        int pilihan;
        do {
            pilihan = inputuser.nextInt();
            switch (pilihan) {
                case 1:
                    System.out.println("\nIdentitas");
                    System.out.println("Nama : Irfan Rifai Aziz");
                    System.out.println("Alasan BE Dev : Ingin menjadi programmer expert");
                    System.out.println("Ekspetasi : Bekerja di perusahaan yang berpengalaman");
                    System.out.println("\n");

                    break;
                case 2:
                    System.out.println("\nKalkulator");
                    System.out.print("\nMasukkan angka pertama : ");
                    int a = inputuser.nextInt();
                    System.out.print("Masukkan angka kedua : ");
                    int b = inputuser.nextInt();
                    System.out.print("Masukkan operasi : ");
                    String operasi = inputuser.next();
                    if (operasi.equals("+")) {
                        System.out.println("Hasilnya : " + (a + b));
                    } else if (operasi.equals("-")) {
                        System.out.println("Hasilnya : " + (a - b));
                    } else if (operasi.equals("*")) {
                        System.out.println("Hasilnya : " + (a * b));
                    } else if (operasi.equals("/")) {
                        System.out.println("Hasilnya : " + (a / b));
                    } else {
                        System.out.println("Operasi tidak ditemukan");
                    }
                    System.out.println("\n");

                    break;
                case 3:
                    System.out.println("\nPerbandingan");
                    System.out.print("\nMasukkan angka pertama : ");
                    int c = inputuser.nextInt();
                    System.out.print("Masukkan angka kedua : ");
                    int d = inputuser.nextInt();
                    if (c > d) {
                        System.out.println("Angka pertama lebih besar dari angka kedua");
                    } else if (c < d) {
                        System.out.println("Angka pertama lebih kecil dari angka kedua");
                    } else {
                        System.out.println("Angka pertama sama dengan angka kedua");
                    }
                    System.out.println("\n");
                    break;
                case 4:
                    System.out.println("\nKeluar");
                    System.exit(0);
                    break;

            }
            System.out.println("Pilihan Fitur :\n1. Identitas \n2. Kalkulator \n3. Perbandingan \n4. Keluar");
            System.out.print("\nPilih fitur : ");

        } while (pilihan < 4);
    }
}