public class TugasDuaIrfan {
    public static void main(String[] args) {
        // nomor 1
        int pertama = pertama(-10);
        System.out.println(pertama);
        // nomor 2
        int[] isiarray = { 1, 2, 3, 4, 45, 6, 7, 8, 9, 20 };
        int kedua = kedua(isiarray);
        System.out.println(kedua);
        // nomor 3
        int ketiga = ketiga(2, 3);
        System.out.println(ketiga);

    }

    public static int ketiga(int a, int b) {
        return (int) Math.pow(a, b);
    }

    public static int kedua(int[] isiarray) {
        int i;
        int max = isiarray[0];
        for (i = 1; i < isiarray.length; i++) {
            if (isiarray[i] > max) {
                max = isiarray[i];
                // return max;
            }

        }
        return max;

    }

    public static int pertama(int a) {
        if (a < 0) {
            return a * (-1);
        } else {
            return a;
        }
    }

}